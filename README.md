[![](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](./LICENSE.md)

Projeto para estágio pós-doutoral na [Universidade Federal de Alagoas - UFAL](https://www.ufal.edu.br/), Brasil.

Projeto apresentado ao [Conselho Nacional de Desenvolvimento Científico e Tecnológico - CNPq](http://www.cnpq.br/web/guest/apresentacao_institucional), atendendo ao *cronograma 1* da [chamada pública](http://www.cnpq.br/web/guest/noticiasviews/-/journal_content/56_INSTANCE_a6MO/10157/5603435) para participação na seleção de projetos para bolsas de pesquisa na modalidade [Pós-Doutorado Júnior - PDJ/CNPq](http://www.cnpq.br/view/-/journal_content/56_INSTANCE_0oED/10157/2958271?COMPANY_ID=10132#PDJ), à ser executado no [Programa de Pós-Graduação em Matemática da Universidade Federal de Alagoas  - PPGMat/UFAL](http://www.im.ufal.br/posgraduacao/posmat/index.php).
